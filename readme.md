Tutorial Inline Input Pada ZKoss Listbox

ZKOSS adalah framework Graphical User Interface GUI untuk Platform Java. Berbeda dengan framework GUI Lain yang berbasis javascript scripting, Zkoss berorientasi serverside programming dengan java. ZKoss membuat java abstraction di sisi server untuk menghasilkan tampilan yang bisa berjalan pada browser.

Informasi lengkap tentang zkoss dapat di akses melalui situs resminya pada www.zkoss.org

Tutorial ini berisi tentang teknik melakukan listing data dan inline editing dengan menggunakan komponen Listbox Zk.

prerequisities:
Prasyarat untuk bisa mencoba tutorial ini pada komputer anda adalah
 
1. jdk 6++ terinstall

2. maven 3.0.4 ++

3. git (msysgit untuk windows)

4. (opsional) java application server: tomcat, glassfish. by default, aplikasi ini dapat running menggunakan jetty melalui maven run goal.

persiapan

0. open command prompt

1. clone project tutorial listinput ini: 

git clone http://bitbucket.org/tejo_ak/listinput-tutorial.git


2. cd to   {working dirrectory}\listinput-tutorial

3. download maven dependency

mvn clean install

4. run dengan maven run

mvn jetty:run

by default, jetty akan running dan aplikasi tutorial dapat diakses melalui

http://localhost:8080/listinput



