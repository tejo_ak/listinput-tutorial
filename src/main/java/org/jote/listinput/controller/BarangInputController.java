package org.jote.listinput.controller;

import org.jote.listinput.midleware.entity.Barang;
import org.jote.listinput.midleware.service.BarangService;
import org.jote.listinput.utility.NextFocusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tejo
 * Date: 4/8/14
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class BarangInputController extends SelectorComposer<Window> {
    public static final String ID_BARANG = "ID_BARANG";
    Logger logger = LoggerFactory.getLogger(BarangInputController.class);
    @Wire
    Listbox lstBarang;
    @Wire
    Button btnAdd, btnRefresh;
    @WireVariable
    BarangService barangService;
    Textbox txtNama, txtKode, txtMerk, txtSatuan;
    Decimalbox txtHarga;
    Listitem liEdit;
    Button btnSimpanInline;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstBarang.setItemRenderer(new ListBarangRenderer());
    }

    public void showInlineData(Barang b) {
        if (b != null) {
            txtNama.setValue(b.getNama());
            txtMerk.setValue(b.getMerk());
            txtKode.setValue(b.getKode());
            txtSatuan.setValue(b.getSatuan());
            txtHarga.setValue(b.getHarga());
        }
    }

    public void renderEditable(Listitem listitem) {
        liEdit = listitem;
        Barang b = listitem.getValue();
        listitem.getChildren().removeAll(listitem.getChildren());

        try {
            new Listcell("-").setParent(listitem);
            Listcell c = new Listcell();
            txtNama = new Textbox();
            txtNama.setHflex("1");
            txtNama.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtKode = new Textbox();
            txtKode.setHflex("1");
            txtKode.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtMerk = new Textbox();
            txtMerk.setHflex("1");
            txtMerk.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtHarga = new Decimalbox();
            txtHarga.setHflex("1");
            txtHarga.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtSatuan = new Textbox();
            txtSatuan.setPlaceholder("<=3 Chars");
            txtSatuan.setHflex("1");
            txtSatuan.setParent(c);
            txtNama.addEventListener("onOK", new NextFocusListener(txtKode));
            txtKode.addEventListener("onOK", new NextFocusListener(txtMerk));
            txtMerk.addEventListener("onOK", new NextFocusListener(txtHarga));
            txtHarga.addEventListener("onOK", new NextFocusListener(txtSatuan));
            c.setParent(listitem);
            showInlineData(b);
            c = new Listcell();
            btnSimpanInline = new Button("Save");
            btnSimpanInline.setHflex("1");
            btnSimpanInline.setParent(c);
            c.setParent(listitem);
            btnSimpanInline.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    simpanInlineEdit();
                    prepareListBarang();
                    btnAdd.focus();
                }
            });
            txtNama.focus();
        } catch (Exception e) {
            String er = String.format("error merender inline edit", e.getMessage());
            logger.error(er, e);
            throw new RuntimeException(er, e);
        }

    }

    public void simpanInlineEdit() {
        if (liEdit == null) {
            return;
        }
        Barang b = liEdit.getValue();
        if (b == null) {
            b = new Barang();
        }
        b.setKode(txtKode.getValue());
        b.setMerk(txtMerk.getValue());
        b.setHarga(txtHarga.getValue());
        b.setNama(txtNama.getValue());
        b.setSatuan(txtSatuan.getValue());
        barangService.simpan(b);
        liEdit.setValue(null);
        txtMerk = null;
        txtNama = null;
        txtHarga = null;
        txtKode = null;
        txtSatuan = null;
//        liEdit = null;
    }

    public void prepareListBarang() {
        List<Barang> bs = barangService.listBarang();
        ListModelList<Barang> lmb = new ListModelList<Barang>(bs, false);
        lstBarang.setModel(lmb);
    }

    @Listen("onClick=#btnAdd")
    public void onAddClick(Event evt) {
        try {
            ListModelList<Barang> lmb = (ListModelList) lstBarang.getModel();
            if (lmb == null) {
                lmb = new ListModelList<Barang>(new ArrayList<Barang>());
                lstBarang.setModel(lmb);
            }
            Barang b = new Barang();
            b.setNama("toEdit");
            lmb.add(null);
        } catch (Exception e) {
            String er = String.format("error adding new inline data %s", e.getMessage());
            logger.error(er, e);
            throw new RuntimeException(er, e);
        }
    }

    @Listen("onClick=#btnRefresh")
    public void onBtnRefreshClick(Event evt) {
        prepareListBarang();
    }

    class ListBarangRenderer implements ListitemRenderer<Barang> {

        @Override
        public void render(Listitem listitem, Barang barang, int i) throws Exception {
            if (barang == null) {
                renderEditable(listitem);
                return;
            }
            new Listcell("" + i).setParent(listitem);
            new Listcell(barang.getNama()).setParent(listitem);
            new Listcell(barang.getKode()).setParent(listitem);
            new Listcell(barang.getMerk()).setParent(listitem);
            new Listcell("" + barang.getHarga()).setParent(listitem);
            new Listcell("" + barang.getSatuan()).setParent(listitem);
            Listcell lc = new Listcell();
            lc.setParent(listitem);
            Hbox h = new Hbox();
            h.setParent(lc);
            Button b = new Button("/");
            b.setTooltiptext("Click to edit this record");
            b.setParent(h);
            b.setAttribute(ID_BARANG, barang.getId());
            b.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    Long id = (Long) event.getTarget().getAttribute(ID_BARANG);
                    if (id != null) {
                        Listitem li = (Listitem) event.getTarget().getParent().getParent().getParent();
                        renderEditable(li);
                    }
                }
            });
            b = new Button("-");
            b.setTooltiptext("Click to delete this record");
            b.setAttribute(ID_BARANG, barang.getId());
            b.setParent(h);
            b.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    Long id = (Long) event.getTarget().getAttribute(ID_BARANG);
                    if (id != null) {
                        Barang b = barangService.getBarang(id);
                        barangService.hapus(b);
                        prepareListBarang();
                    }
                }
            });
            listitem.setValue(barang);
        }
    }

}
