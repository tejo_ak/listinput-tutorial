package org.jote.listinput.controller;

import org.jote.listinput.midleware.entity.Barang;
import org.jote.listinput.midleware.entity.Penjualan;
import org.jote.listinput.midleware.service.BarangService;
import org.jote.listinput.midleware.service.PenjualanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tejo
 * Date: 4/8/14
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class PenjualanInputController extends SelectorComposer<Window> {
    public static final String BANDBOX_UUID = "BANDBOX_UUID";
    public static final String ID_BARANG = "ID_BARANG";
    Logger logger = LoggerFactory.getLogger(PenjualanInputController.class);
    public static final String ID_PENJUALAN = "ID_PENJUALAN";
    @Wire
    Listbox lstPenjualan;

    @Wire
    Button btnAdd, btnRefresh;

    @WireVariable
    PenjualanService penjualanService;
    @WireVariable
    BarangService barangService;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstPenjualan.setItemRenderer(new ListPenjualanRenderer());
    }

    Bandbox txtKode;
    Textbox txtMerk;
    Label txtBarang, txtHarga, txtTotal;
    Longbox txtQty;
    Listitem liEdit;
    Button btnSimpanInline;

    public void showInlineData(Penjualan b) {
        if (b != null) {
//            txtNama.setValue(b.getNama());
            txtKode.setValue(b.getBarang().getKode());
            txtHarga.setValue("" + b.getBarang().getHarga());
            txtBarang.setValue("" + b.getBarang().getNama());
            txtQty.setValue(b.getJumlah());
            txtTotal.setValue(""+b.getTotal());
            txtKode.setAttribute(ID_BARANG,b.getBarang().getId());
            txtKode.focus();
        }
    }


    public void renderEditable(Listitem listitem) {
        liEdit = listitem;
        Penjualan b = listitem.getValue();
        listitem.getChildren().removeAll(listitem.getChildren());

        try {
            new Listcell("-").setParent(listitem);
            Listcell c = new Listcell();
            txtKode = createBandboxKode();
            txtKode.setHflex("1");
            txtKode.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtBarang = new Label();
            txtBarang.setHflex("1");
            txtBarang.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtQty = new Longbox();
            txtQty.setHflex("1");
            txtQty.setParent(c);
            txtQty.addEventListener("onOK", new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    if (txtHarga.getValue() != null && !"".equals(txtHarga.getValue())) {
                        BigDecimal b = calculateTotal(txtQty.getValue(), new BigDecimal(txtHarga.getValue()));
                        txtTotal.setValue("" + b);
                    }
                    btnSimpanInline.focus();
                }
            });
            c.setParent(listitem);

            c = new Listcell();
            txtHarga = new Label();
            txtHarga.setHflex("1");
            txtHarga.setParent(c);
            c.setParent(listitem);
            c = new Listcell();
            txtTotal = new Label();
            txtTotal.setHflex("1");
            txtTotal.setParent(c);
            c.setParent(listitem);
            showInlineData(b);
            c = new Listcell();
            btnSimpanInline = new Button("Save");
            btnSimpanInline.setHflex("1");
            btnSimpanInline.setParent(c);
            c.setParent(listitem);
            btnSimpanInline.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    simpanInlineEdit();
                    prepareListPenjualan();
                    btnAdd.focus();
                }
            });
            txtKode.focus();
        } catch (Exception e) {
            String er = String.format("error merender inline edit", e.getMessage());
            logger.error(er, e);
            throw new RuntimeException(er, e);
        }

    }


    public void simpanInlineEdit() {
        if (liEdit == null) {
            return;
        }
        Penjualan b = liEdit.getValue();
        if (b == null) {
            b = new Penjualan();
        }
        b.setJumlah(txtQty.getValue());
        if (txtTotal.getValue() != null && !"".equals(txtTotal.getValue())) {
            b.setTotal(new BigDecimal(txtTotal.getValue()));
        }
        b.setBarang(barangService.getBarangOfKode(txtKode.getValue()));
        penjualanService.simpan(b);
        liEdit.setValue(null);
        txtMerk = null;
//        txtNama = null;
        txtHarga = null;
        txtKode = null;
//        txtSatuan = null;
//        liEdit = null;
    }

    public void prepareListPenjualan() {
        List<Penjualan> bs = penjualanService.listPenjualan();
        ListModelList<Penjualan> lmb = new ListModelList<Penjualan>(bs, false);
        lstPenjualan.setModel(lmb);
    }

    @Listen("onClick=#btnAdd")
    public void onAddClick(Event evt) {
        try {
            ListModelList<Barang> lmb = (ListModelList) lstPenjualan.getModel();
            if (lmb == null) {
                lmb = new ListModelList<Barang>(new ArrayList<Barang>());
                lstPenjualan.setModel(lmb);
            }
            Barang b = new Barang();
            b.setNama("toEdit");
            lmb.add(null);
        } catch (Exception e) {
            String er = String.format("error adding new inline data %s", e.getMessage());
            logger.error(er, e);
            throw new RuntimeException(er, e);
        }
    }

    @Listen("onClick=#btnRefresh")
    public void onBtnRefreshClick(Event evt) {
        prepareListPenjualan();
    }

    private BigDecimal calculateTotal(Penjualan p) {
        if (p == null || p.getBarang() == null || p.getJumlah() == null || p.getBarang().getHarga() == null) {
            return BigDecimal.ZERO;
        }
        return calculateTotal(p.getJumlah(), p.getBarang().getHarga());
    }

    private BigDecimal calculateTotal(Long jumlah, BigDecimal harga) {
        double d = jumlah.doubleValue() * harga.doubleValue();
        return new BigDecimal(d);
    }

    private void resolveBarang(String kode) {
        Barang brg = barangService.getBarangOfKode(kode);
        if (brg != null) {
            txtBarang.setValue(brg.getNama());
            txtHarga.setValue("" + brg.getHarga());
        }
    }

    private Bandbox createBandboxKode() {
        Bandbox b = new Bandbox();
        b.addEventListener("onOK", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                resolveBarang(((Bandbox) event.getTarget()).getValue());
                txtQty.focus();
            }
        });
        b.setButtonVisible(true);
        b.setAutodrop(true);
        Bandpopup bp = new Bandpopup();
        bp.setWidth("300px");
        bp.setParent(b);
        Listbox l = new Listbox();
        l.setParent(bp);
        l.setAttribute(BANDBOX_UUID, b.getUuid());
        Listhead lh = new Listhead();
        lh.setParent(l);
        Listheader lhd = new Listheader("Kode");
        lhd.setHflex("4");
        lhd.setParent(lh);
        lhd = new Listheader("Barang");
        lhd.setHflex("6");
        lhd.setParent(lh);
        lhd = new Listheader("Harga");
        lhd.setHflex("5");
        lhd.setParent(lh);
        l.setItemRenderer(new ListBarangRenderer());
        List<Barang> brgs = barangService.listBarang();
        ListModelList<Barang> lmb = new ListModelList<Barang>(brgs, true);
        l.setModel(lmb);
        l.addEventListener("onSelect", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {

                Listbox l = (Listbox) event.getTarget();
                Bandbox b = (Bandbox) l.getParent().getParent();
                Barang br = ((Barang) l.getSelectedItem().getValue());
                b.setValue(br.getKode());
                b.setAttribute(ID_BARANG, br.getId());
                resolveBarang(b.getValue());
                txtQty.focus();
                b.close();
            }
        });
        return b;
    }

    class ListPenjualanRenderer implements ListitemRenderer<Penjualan> {

        @Override
        public void render(Listitem listitem, Penjualan penjualan, int i) throws Exception {
            if (penjualan == null) {
                renderEditable(listitem);
                return;
            }
            boolean barangExists = penjualan.getBarang() != null;
            new Listcell("" + i).setParent(listitem);
            new Listcell(barangExists ? penjualan.getBarang().getKode() : "").setParent(listitem);
            new Listcell(barangExists ? penjualan.getBarang().getNama() : "").setParent(listitem);
            new Listcell("" + penjualan.getJumlah()).setParent(listitem);
            new Listcell(barangExists ? "" + penjualan.getBarang().getHarga() : "").setParent(listitem);
            new Listcell("" + calculateTotal(penjualan)).setParent(listitem);
            Listcell lc = new Listcell();
            lc.setParent(listitem);
            Hbox h = new Hbox();
            h.setParent(lc);
            Button b = new Button("/");
            b.setTooltiptext("Click to edit this record");
            b.setParent(h);
            b.setAttribute(ID_PENJUALAN, penjualan.getId());
            b.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    Long id = (Long) event.getTarget().getAttribute(ID_PENJUALAN);
                    if (id != null) {
                        Listitem li = (Listitem) event.getTarget().getParent().getParent().getParent();
                        renderEditable(li);
                    }
                }
            });
            b = new Button("-");
            b.setTooltiptext("Click to delete this record");
            b.setAttribute(ID_PENJUALAN, penjualan.getId());
            b.setParent(h);
            b.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    Long id = (Long) event.getTarget().getAttribute(ID_PENJUALAN);
                    if (id != null) {
                        Penjualan b = penjualanService.getPenjualan(id);
                        penjualanService.hapus(b);
                        prepareListPenjualan();
                    }
                }
            });
            listitem.setValue(penjualan);
        }
    }


    class ListBarangRenderer implements ListitemRenderer<Barang> {

        @Override
        public void render(Listitem listitem, Barang barang, int i) throws Exception {
//            new Listcell("" + i).setParent(listitem);
            new Listcell(barang.getKode()).setParent(listitem);
            new Listcell(barang.getNama()).setParent(listitem);
            new Listcell("" + barang.getHarga()).setParent(listitem);
            listitem.setValue(barang);
        }
    }
}
