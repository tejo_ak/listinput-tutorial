package org.jote.listinput.midleware.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tejo
 * Date: 4/8/14
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Barang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 64)
    private String nama;
    @Column(length = 6)
    private String kode;
    private BigDecimal harga;
    @Column(length = 3)
    private String satuan;
    @Column(length = 32)
    private String merk;

    @OneToMany(mappedBy = "barang", targetEntity = Penjualan.class, fetch = FetchType.LAZY)
    private List<Penjualan> penjualans;

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public List<Penjualan> getPenjualans() {
        return penjualans;
    }

    public void setPenjualans(List<Penjualan> penjualans) {
        this.penjualans = penjualans;
    }
}
