package org.jote.listinput.midleware.service;

import org.jote.listinput.midleware.entity.Barang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tejo
 * Date: 4/8/14
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("barangService")
public class BarangService {
    Logger logger = LoggerFactory.getLogger(BarangService.class);

    @PersistenceContext
    EntityManager em;

    @Transactional
    public Barang simpan(Barang barang) {
        String kode = barang.getKode();
        if (kode == null || "".equals(kode)) {
            kode = UUID.randomUUID().toString().substring(1, 4).toUpperCase();
            barang.setKode(kode);
        }
        Barang b = em.merge(barang);
        em.flush();
        em.refresh(b);
        return b;
    }

    @Transactional
    public void hapus(Barang barang) {
        barang = em.merge(barang);
        try {
            em.remove(barang);
            em.flush();
        } catch (Exception e) {
            String er = String.format("error menghapus barang karena: ", e.getMessage());
            logger.error(er, e);
            throw new RuntimeException(er, e);
        }
    }

    public List<Barang> listBarang() {
        String sList = String.format("select b from Barang b");
        Query qList = em.createQuery(sList, Barang.class);
        return qList.getResultList();
    }

    public Barang getBarangOfKode(String kode) {
        String sList = String.format("select b from Barang b where b.kode='%s'", kode);
        Query qList = em.createQuery(sList, Barang.class);
        List<Barang> brgs = qList.getResultList();
        for (Iterator<Barang> iterator = brgs.iterator(); iterator.hasNext(); ) {
            Barang brg = iterator.next();
            return brg;
        }
        return null;
    }

    public Barang getBarang(Long id) {
        return em.find(Barang.class, id);
    }
}
