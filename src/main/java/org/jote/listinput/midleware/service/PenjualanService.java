package org.jote.listinput.midleware.service;

import org.jote.listinput.midleware.entity.Barang;
import org.jote.listinput.midleware.entity.Penjualan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: tejo
 * Date: 4/8/14
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("penjualanService")
public class PenjualanService {
    Logger logger = LoggerFactory.getLogger(PenjualanService.class);


    @PersistenceContext
    EntityManager em;

    @Transactional
    public Penjualan simpan(Penjualan penjualan) {
        Penjualan b = em.merge(penjualan);
        em.flush();
        em.refresh(b);
        return b;
    }

    @Transactional
    public void hapus(Penjualan penjualan) {
        penjualan = em.merge(penjualan);
        try {
            em.remove(penjualan);
            em.flush();
        } catch (Exception e) {
            String er = String.format("error menghapus penjualan karena: ", e.getMessage());
            logger.error(er, e);
            throw new RuntimeException(er, e);
        }
    }

    public List<Penjualan> listPenjualan() {
        String sList = String.format("select p from Penjualan p, Barang b where p.barang=b");
        Query qList = em.createQuery(sList, Penjualan.class);
        return qList.getResultList();
    }

    public Penjualan getPenjualan(Long id) {
        return em.find(Penjualan.class, id);
    }


}
