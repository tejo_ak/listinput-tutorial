package org.jote.listinput.utility;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: lenovospy
 * Date: 4/9/14
 * Time: 6:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class NextFocusListener implements EventListener<Event> {
    HtmlBasedComponent c;

    public NextFocusListener(HtmlBasedComponent toFocus) {
        this.c = toFocus;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        c.focus();
    }
}
